import {StyleSheet} from 'react-native'
const styles = StyleSheet.create({
    viewGlobal :{
        flex:1,
        backgroundColor:"black"
    },

    titleScreen : {
        fontSize : 20,
        color:"white",
        textAlign:'center',
        borderBottomWidth:1,
        borderBottomColor:'#cdcdcd',
        height:40,
    }
})
export default styles;