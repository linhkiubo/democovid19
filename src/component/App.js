import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Global from '../screens/Global';
import Countries from '../screens/Countries';
import {NavigationContainer} from '@react-navigation/native';
import images from '../assets/images/images';

export default class App extends Component {
  render() {
    const Tab = createBottomTabNavigator();
    function MyTab() {
      return (
        <Tab.Navigator initialRouteName="Global">
          <Tab.Screen
            name="Global"
            component={Global}
            options={{
              tabBarIcon: ({focused}) =>
                focused ? (
                  <Image
                    source={images.global}
                    style={[{width: 25, height: 25}]}
                  />
                ) : (
                  <Image
                    source={images.globalDrak}
                    style={[{tintColor: '#000', width: 15, height: 15}]}
                  />
                ),
            }}
          />
          <Tab.Screen
            name="Countries"
            component={Countries}
            options={{
              tabBarIcon: ({focused}) =>
                focused ? (
                  <Image
                    source={images.flag}
                    style={[{width: 25, height: 25}]}
                  />
                ) : (
                  <Image
                    source={images.flagDark}
                    style={[{width: 20, height: 20}]}
                  />
                ),
            }}
          />
        </Tab.Navigator>
      );
    }
    return (
      <NavigationContainer>
        <MyTab />
      </NavigationContainer>
    );
  }
}
