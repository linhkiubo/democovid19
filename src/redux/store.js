import {createStore} from 'redux';
import React, {Component} from 'react';
import {Provider} from 'react-redux';
import App from '../component/App.js';
const dafaultState = {
  country: null,
  cases: null,
  todayCases: null,
  deaths: null,
  todayDeaths: null,
  recovered: null,
  active: null,
  critical: null,
  casesPerOneMillion: null,
  deathsPerOneMillion: null,
  firstCase: null,
  isLoading: false,
  error: false,
};
const reducer = (state = dafaultState, action) => {
  return state;
};

const store = createStore(reducer);

export default class Store extends Component {
  render() {
    return (
      <Provider store={store}>
        <App />
      </Provider>
    );
  }
}
