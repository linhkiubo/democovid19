const images = {
    flag :  require('./flag.png'),
    flagDark: require('./flagDark.png'),
    global: require('./global.png'),
    globalDrak: require('./globalDark.png')
};
export default images;