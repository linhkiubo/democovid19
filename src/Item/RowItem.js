import React, {Component} from 'react';
import {Text, View} from 'react-native';

export default function RowItem({item}) {
  return (
    <View style={{width:'100%',height:300}}>
      <Text style={{color: 'white'}}>{item.country}</Text>
      <Text style={{color: 'white'}}>{item.cases}</Text>
      <Text style={{color: 'white'}}>{item.todayCases}</Text>
      <Text style={{color: 'white'}}>{item.deaths}</Text>
      <Text style={{color: 'white'}}>{item.todayDeaths}</Text>
      <Text style={{color: 'white'}}>{item.recovered}</Text>
      <Text style={{color: 'white'}}>{item.active}</Text>
      <Text style={{color: 'white'}}>{item.critical}</Text>
      <Text style={{color: 'white'}}>{item.casesPerOneMillion}</Text>
      <Text style={{color: 'white'}}>{item.deathsPerOneMillion}</Text>
      <Text style={{color: 'white'}}>{item.testsPerOneMillion}</Text>
      <Text style={{color: 'white'}}>{item.totalTests}</Text>
    </View>
  );
}
