import React, {Component, useEffect, useState} from 'react';
import {Text, View, Button} from 'react-native';
import styles from '../styles/styles';
import axios from 'axios';
import {connect} from 'react-redux';

function Global() {
  const [cases, setCases] = useState('');
  useEffect(() => {
    axios({
      url: 'https://coronavirus-19-api.herokuapp.com/all',
      method: 'get',
    })
      .then(res=> setCases(res.data.cases),)
      .catch(function(err) {
        console.log(err);
      });
  });
  return (
    <View style={styles.viewGlobal}>
      <View style={styles.viewGlobal}>
        <Text style={styles.titleScreen}>Global Virus CORONA Information</Text>

        <Text style={{color: 'white'}}>{cases}</Text>
      </View>
    </View>
  );
}
function mapStateToProps(state) {
  return {myLoading: state.isLoading};
}
export default connect(mapStateToProps)(Global);
