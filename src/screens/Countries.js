import React, {useState, useEffect} from 'react';
import {View, Text, FlatList, Button, TextInput} from 'react-native';
import styles from '../styles/styles';
import axios from 'axios';
import RowItem from '../Item/RowItem';

export default function Countries() {
  const [data, setData] = useState(null);
  const [country, setCountry] = useState('');
  useEffect(() => {
    let url = `https://coronavirus-19-api.herokuapp.com/countries/${country}`;
    console.log(url);
    axios({
      url,
      method: 'GET',
    })
      .then((res) => {
        console.log(res.data), setData(res.data);
      })
      .catch((err) => console.log(err));
  }, [country]);

  return (
    <View style={styles.viewGlobal}>
      <View style={styles.viewGlobal}>
        <Text style={styles.titleScreen}>Global Virus CORONA Information</Text>
        <TextInput
          placeholder="Typing Country Name"
          placeholderTextColor="white"
          onChangeText={(text) => setCountry(text)}
          style={{
            borderWidth: 1,
            borderColor: 'white',
            marginTop: 20,
            color: 'white',
          }}
        />
        {/* {Array.isArray(data) ? (
          <FlatList
          data={data}
          renderItem={({item}) => <RowItem item={item} />}
          keyExtractor={(item) => item.country}
        />
        ) ?
        data === 'Country not found' ? (
          <Text>Tên quốc gia không đúng</Text>
        ) : (
        <Text style={{color:'white'}}>{data}</Text>
        )
      } */}

        {data === 'Country not found' ? (
          <Text style={{color: 'white'}}>Tên quốc gia không đúng</Text>
        ) : Array.isArray(data) ? (
          <FlatList
            data={data}
            renderItem={({item}) => <RowItem item={item} />}
            keyExtractor={(item) => item.country}
          />
        ) : (
          <View style={{width:'100%',height:300}}>
            <Text style={{color: 'white'}}>{data.country}</Text>
            <Text style={{color: 'white'}}>{data.active}</Text>
          </View>
        )}
      </View>
    </View>
  );
}

// const App = () => {
//   const [data, setData] = useState('');
//   const [country, setCountry] = useState('');
//   const [search, setSearch] = useState('');
//   useEffect(() => {
//     let url = `https://coronavirus-19-api.herokuapp.com/countries/${search}`;
//     console.log('url', url);
//     axios({
//       url,
//       method: 'get',
//     })
//       .then((res) => setData(res.data))
//       .catch((err) => console.log(err));
//   }, [search]);
//   console.log(data);
//   console.log(country);

//   return (
//     <View style={styles.viewGlobal}>
//       <View style={styles.viewGlobal}>
//         <Text style={styles.titleScreen}>Global Virus CORONA Information</Text>
//         <TextInput
//           placeholder="Typing Country Name"
//           placeholderTextColor="white"
//           onChangeText={(text) => setCountry(text)}
//           style={{
//             borderWidth: 1,
//             borderColor: 'white',
//             marginTop: 20,
//             color: 'white',
//           }}
//         />
//         <Button title="Search" onPress={() => setSearch(country)} />
//         {data === 'Country not found' ? <Text>Tên quốc gia không đúng</Text> : <FlatList
//           data={data}
//           renderItem={({item}) => <RowItem item={item} />}
//           keyExtractor={item => item.country}
//         />}
//       </View>
//     </View>
//   );
// };
// export default App;
